import 'package:audioplayers/audioplayers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() => runApp(XylophoneApp());

class XylophoneApp extends StatelessWidget {
  final player = AudioCache();

  tocarSom(int nota) {
    player.play('note$nota.wav');
  }

  Widget xylophone(int notaMusical, color) {
    return Expanded(
        child: ElevatedButton(
      style: ElevatedButton.styleFrom(primary: color),
      onPressed: () {
        tocarSom(notaMusical);
      },
      child: null,
    ));
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: SafeArea(
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: [
                xylophone(1, Colors.black),
                xylophone(2, Colors.orange),
                xylophone(3, Colors.red),
                xylophone(4, Colors.green),
                xylophone(5, Colors.blue),
                xylophone(6, Colors.yellow),
                xylophone(7, Colors.grey),

              ],
            ),
          ),
        ),
      ),
    );
  }
}
